/*
 * model-editor.h
 *
 * GUI for editing atomic models
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifndef MODEL_EDITOR_H
#define MODEL_EDITOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

typedef struct {

	struct struct_atomicmodel *model;
	
	GtkWidget *window;
	GtkListStore *list_store;
	
	GtkWidget *thickness;	/* Text entry for thickness */
	GtkWidget *symmetry;	/* GtkSymmetry for model */
	GtkWidget *point_atoms;
	
	GtkWidget *model_atoms_tree;
	
	GtkWidget *add_button;
	GtkWidget *atom_define;
	GtkWidget *delete_button;
	
	GtkWidget *position_table;
	
	GtkWidget *x_scale;
	GtkWidget *x_edit;
	
	GtkWidget *y_scale;
	GtkWidget *y_edit;
	
	GtkWidget *z_scale;
	GtkWidget *z_edit;

	GtkWidget *b_scale;
	GtkWidget *b_edit;

	GtkWidget *occ_scale;
	GtkWidget *occ_edit;
	
	int scale_lock;			/* Don't respond to scale value-changed if this >1 */

} ModelEditor;

extern ModelEditor *model_editor_open(struct struct_atomicmodel *model);
extern void model_editor_get_model(ModelEditor *editor);

extern gint model_load_open(GtkWidget *widget, gpointer data);
extern gint model_save_open(GtkWidget *widget, gpointer data);
extern void model_editor_update(struct struct_atomicmodel *model);
extern void model_editor_lock(ModelEditor *editor);
extern void model_editor_unlock(ModelEditor *editor);

#endif	/* MODEL_EDITOR_H */

