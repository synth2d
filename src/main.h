/*
 * main.h
 *
 * The Top Level Source File
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef MAIN_H
#define MAIN_H

#include <gtk/gtk.h>

#include "reflist.h"
#include "symmetry.h"
#include "geometry.h"
#include "refine.h"
#include "cdm.h"
#include "cflip.h"


extern void main_show_patterson(void);
extern void main_show_pattersone(void);
extern void main_show_knownphases(void);
extern void main_show_calcphases(void);
extern void main_show_difference(void);
extern void main_show_refsyn(void);
extern void main_show_diffpatt(void);

extern void main_wilsonplot(void);
extern void main_falloffplot(void);
extern void main_dpsynth(void);
extern void main_dpsynth_update(void);
extern void main_argand(void);
extern void main_argand_update(void);
extern void main_dethermalise(double level);
extern void main_normalise_exponential(double a, double b, double c);
extern void main_normalise(double level);
extern void main_symmetrise(Symmetry symmetry);
extern void main_gsf_initialise();
extern void main_gsf_reset();
extern unsigned int main_cdm_tangentexpansion(CDMContext *cdm);
extern void main_aperture_open(void);
extern void main_display_phasing_solution(CDMContext *cdm, PhasingSolution *sol);
extern void main_savereflections(const char *filename);

extern void main_substitutereflections(ReflectionList *new);
extern void main_geometry_correct(GeometryCorrectionType correction_type, double wavenumber, double circleradius);
extern void main_displayr(void);
extern void main_superlattice_split(unsigned int xc, unsigned int yc);
extern void main_hpfilter(void);

extern ReflectionList *main_reflist(void);

extern unsigned int main_max_h(void);
extern unsigned int main_max_k(void);

extern void main_stripzero(void);
extern void main_antialias(void);

#endif	/* MAIN_H */

