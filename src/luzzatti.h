/*
 * luzzatti.h
 *
 * Plot of R-factor against resolution
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef LUZZATTI_H
#define LUZZATTI_H

extern void luzzatti_show(void);

#endif	/* LUZZATTI_H */
