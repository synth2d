/*
 * geometry.h
 *
 * Geometrical (Lorentz) Corrections
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "reflist.h"

typedef enum {
	GEOMETRY_CORRECTION_TYPE_VINCENT_MIDGLEY,
	GEOMETRY_CORRECTION_TYPE_GJONNES,
	GEOMETRY_CORRECTION_TYPE_GJONNES_DIVERGENCE
} GeometryCorrectionType;

extern void geometry_correct(ReflectionList *reflections, GeometryCorrectionType correction_type, double wavenumber, double R);
extern void geometry_dialog_open(void);

#endif	/* GEOMETRY_H */
