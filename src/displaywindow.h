/*
 * displaywindow.h
 *
 * The main display window
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef DISPLAYWINDOW_H
#define DISPLAYWINDOW_H

#include <gtk/gtk.h>
#include <fftw3.h>

#include "reflist.h"

typedef enum {
	DWR_NONE,
	DWR_CLEAN,
	DWR_GSF,
	DWR_ELSER
} DisplayWindowRealSpace;

extern GtkWidget *displaywindow_gtkwindow(void);
extern int displaywindow_mode(void);
extern void displaywindow_open(const char *filenameull);
extern void displaywindow_statusbar(const char *message);
extern void error_report(const char *message);
extern void displaywindow_show_patterson(ReflectionList *reflections);
extern void displaywindow_show_pattersone(ReflectionList *reflections);
extern void displaywindow_show_knownphases(ReflectionList *reflections);
extern void displaywindow_show_calcphases(ReflectionList *reflections);
extern void displaywindow_show_difference(ReflectionList *reflections);
extern void displaywindow_show_refsyn(ReflectionList *reflections);
extern void displaywindow_show_diffpatt(ReflectionList *reflections);
extern void displaywindow_switchview(void);
extern void displaywindow_set_realspace(fftw_complex *out, DisplayWindowRealSpace rs);
extern double displaywindow_maxpeak(void);
extern double displaywindow_max(void);
extern fftw_complex *displaywindow_outarray(void);
extern void displaywindow_createfourier(void);
extern void displaywindow_forceview(int new_view);
extern void displaywindow_disablephasegrabbing(void);
extern void displaywindow_enablephasegrabbing(void);
extern void displaywindow_set_gamma(double gamma);
extern GdkPixbuf *displaywindow_render_pixbuf(fftw_complex *out, double brightness,
				size_t width_o, size_t height_o, double gamma, int nx, int ny);
extern gint displaywindow_brightness_auto(GtkWidget *widget, gpointer data);
extern fftw_complex *displaywindow_outarray(void);
extern void displaywindow_kicksize(void);

enum {
	DWV_PATTERSON,
	DWV_PATTERSONE,
	DWV_KNOWNPHASE,
	DWV_CALCPHASE,
	DWV_REALSPACE,
	DWV_MODEL,
	DWV_DIFFERENCE,
	DWV_REFSYN,
	DWV_DIFFPATT,
	DWV_SIMPATT,
	DWV_SIMFOLZPATT,
	DWV_EXITWAVE
};

#endif	/* DISPLAYWINDOW_H */

