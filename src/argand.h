/*
 * argand.h
 *
 * "Argand plane tracking"
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef ARGAND_H
#define ARGAND_H

#include "reflist.h"

extern void argand_update(ReflectionList *reflections);
extern void argand_open(ReflectionList *reflections);

#endif	/* ARGAND_H */

