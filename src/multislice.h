/*
 * multislice.h
 *
 * Multislice Dynamical Simulations
 *
 * (c) 2006-2009 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef MULTISLICE_H
#define MULTISLICE_H

#include "model.h"
#include "reflist.h"

extern ReflectionList *multislice_calculate_f_dyn(AtomicModel *model,
						  ReflectionList *template);

#endif	/* MULTISLICE_H */
