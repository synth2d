/*
 * dpsynth.h
 *
 * Draw synthetic diffracion patterns
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef DPSYNTH_H
#define DPSYNTH_H

typedef struct {
	GtkWidget *image_widget;
	GdkPixbuf *pixbuf;
	GtkWidget *window;
	unsigned int width;
	unsigned int height;
	unsigned int colour;
	
} DPSynthWindow;

extern void dpsynth_main_update(ReflectionList *reflections);
extern void dpsynth_main_open(ReflectionList *reflections);
extern DPSynthWindow *dpsynth_open(ReflectionList *reflections, const char *title, int colour);
extern void dpsynth_update(DPSynthWindow *dpsynth, ReflectionList *reflections);

extern void dpsynth_simdp_open(ReflectionList *reflections);
extern void dpsynth_simdp_update(ReflectionList *reflections);

#endif	/* DPSYNTH_H */

