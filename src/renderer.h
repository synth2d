/*
 * renderer.c
 *
 * Render Fourier Transform output array into display array
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2D - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef RENDERER_H
#define RENDERER_H

typedef struct {
	double *re;
	double *im;
} ComplexArray;

extern double renderer_width(int width, int height, double gamma, int nx, int ny);
extern double renderer_height(int width, int height, double gamma, int nx, int ny);
extern ComplexArray renderer_draw(fftw_complex *out, int width, int height, double gamma, int nx, int ny);
extern double renderer_map_x(double x, double y, int width, int height, double gamma, int nx, int ny);
extern double renderer_map_y(double x, double y, int width, int height, double gamma, int nx, int ny);

#endif	/* RENDERER_H */

