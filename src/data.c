/*
 * data.c
 *
 * Handle the input data
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <string.h>

#include "main.h"
#include "data.h"
#include "reflist.h"
#include "displaywindow.h"

signed int data_width_hidden = 512;
signed int data_height_hidden = 512;
float data_a_hidden = 1;
float data_b_hidden = 1;
float data_c_hidden = 1;
double data_gamma_hidden = M_PI_2;
ReflectionList *data_reflections = NULL;
int data_image_scale = 500;

unsigned int data_width() { return data_width_hidden; }
unsigned int data_height() { return data_height_hidden; }
double data_a() { return data_a_hidden; }
double data_b() { return data_b_hidden; }
double data_c() { return data_c_hidden; }
double data_gamma() { return data_gamma_hidden; }
unsigned int data_get_image_scale() { return data_image_scale; }

static void data_calculatesize() {
	data_width_hidden = data_a_hidden*data_image_scale;
	data_height_hidden = data_b_hidden*data_image_scale;
}

void data_dividecell(unsigned int na, unsigned int nb, unsigned int nc) {
	data_a_hidden /= na;
	data_b_hidden /= nb;
	data_c_hidden /= nc;
	data_calculatesize();
}

int data_read(const char *filename) {

	FILE *fh;
	char *line;
	char *rval;
	unsigned int whoops = 0;
	float gamma_angle = 90;
	signed int laue = 0;

	if ( data_reflections ) {
		free(data_reflections);
	}
	data_reflections = reflist_new();

	fh = fopen(filename, "r");
	if ( !fh ) {
		fprintf(stderr, "DA: Couldn't open file %s\n", filename);
		return 1;
	}
	line = malloc(1024);
	rval = "hello";
	while ( rval ) {

		signed int h, k, l;
		float am, intensity;
		float ph = 0;
		int res;

		rval = fgets(line, 1023, fh);
		if ( ferror(fh) && !feof(fh) ) {
			whoops = 1;
			break;
		}
		if ( strlen(line) > 1 ) {
			if ( line[strlen(line)-1] == '\n' ) {
				line[strlen(line)-1] = '\0';	/* Cut off trailing newline. */
			}
		} else {
			line[0] = '\0';
		}

		res = sscanf(line, "%3i %3i %3i %f %f", &h, &k, &l, &intensity, &ph);
		if ( intensity > 0.0) am = sqrt(intensity);
		if ( (res < 4) && gamma_angle ) {
			sscanf(line, "angle %f", &gamma_angle);
			sscanf(line, "a %f", &data_a_hidden);
			sscanf(line, "b %f", &data_b_hidden);
			sscanf(line, "c %f", &data_c_hidden);
			sscanf(line, "scale %i", &data_image_scale);
		} else {
			if ( res == 5 ) {

				if ( (h==0) && (k==0) && (l==0) ) {
					printf("DA: Data contains a zero-order beam value.\n");
				} else if ( (h==0) && (k==0) ) {
					printf("DA: Data may contain a zero-order beam value.\n");
				}
				if ( (l != laue) && (laue != 999999) ) {
					//printf("DA: WARNING: Data is from more than one Laue zone\n");
				} else {
					laue = l;
					reflist_addref_am_ph(data_reflections, h, k, l, am, ph);
				}
			} else {
				if ( (h==0) && (k==0) && (l==0) ) {
					printf("DA: Data contains a zero-order beam value.\n");
				} else if ( (h==0) && (k==0) ) {
					printf("DA: Data may contain a zero-order beam value.\n");
				}
				if ( (l != laue) && (laue != 999999) ) {
					//printf("DA: WARNING: Data is from more than one Laue zone\n");
				} else {
					laue = l;
					reflist_addref_am(data_reflections, h, k, l, am);
				}
			}
		}

		line[0] = '\0';	/* Prevent this line from being parsed twice. */

	}

	fclose(fh);
	free(line);

	if ( whoops ) {
		fprintf(stderr, "Couldn't read file %s\n", filename);
		return 1;
	}

	data_calculatesize();
	printf("a=%f, b=%f, width=%i, height=%i, gamma=%f\n", data_a_hidden, data_b_hidden, data_width_hidden, data_height_hidden, gamma_angle);
	data_gamma_hidden = (gamma_angle/180)*M_PI;

	return 0;

}

ReflectionList *data_getreflections() {
	return data_reflections;
}

void data_free() {

	reflist_free(data_reflections);

}
