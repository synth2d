/*
 * options.h
 *
 * Handle run-time options and stuff
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef OPTIONS_H
#define OPTIONS_H

extern void options_load(void);
extern void options_save(void);

#endif	/* OPTIONS_H */

