/*
 * normalise.h
 *
 * Normalisation stuff
 *
 * (c) 2006-2009 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef NORMALISE_H
#define NORMALISE_H

#include "reflist.h"

extern void normalise_dialog_open(void);
extern void normalise_wilsonplot(ReflectionList *reflections);
extern void normalise_falloffplot(ReflectionList *reflections);
extern void normalise_dethermalise_open(void);
extern void normalise_exponential_open(void);
extern void normalise_exponential(ReflectionList *reflections, double a, double b, double c);
extern void normalise_dethermalise(ReflectionList *reflections, double level);
extern void normalise_execute(ReflectionList *reflections, double level);
extern double resolution(signed int h, signed int k, signed int l,
			 double a, double b, double c, double gamma);

#endif	/* NORMALISE_H */
