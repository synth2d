/*
 * elements.c
 *
 * Elemental Data
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2D - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef ELEMENTS_H
#define ELEMENTS_H

typedef struct {
	char *element_name;
	unsigned int z;
	/* Units of Angstroms and Degrees */
	double sfac_a1; double sfac_b1; double sfac_a2; double sfac_b2;
	double sfac_a3; double sfac_b3; double sfac_a4; double sfac_b4;
	double sfac_c;
} Element;
extern Element elements[255];

extern void elements_initialise(void);
extern unsigned int elements_lookup(const char *name);

#endif	/* ELEMENTS_H */

