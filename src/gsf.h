/*
 * gsf.h
 *
 * Gerchberg-Saxton-Fienup and other Projection iteration algorithms
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef GSF_H
#define GSF_H

#include "reflist.h"
#include "symmetry.h"

extern void gsf_dpsynth_main_update(ReflectionList *gsf_listout);
extern void gsf_dpsynth_main_open(ReflectionList *gsf_listout);

extern void gsf_dialog_open(void);
extern void gsf_initialise(ReflectionList *reflections);
extern void gsf_reset(ReflectionList *reflections);
extern void gsf_mask(ReflectionList *reflections, fftw_complex *in, unsigned int width, unsigned int height);

#endif	/* GSF_H */
