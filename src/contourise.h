/*
 * contourise.h
 *
 * Draw contour maps
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CONTOURISE_H
#define CONTOURISE_H

extern void contourise_dialog_open(int nx, int ny);

#endif	/* CONTOURISE_H */
