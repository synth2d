/*
 * refine-cgrad.h
 *
 * Refinement by Conjugate Gradient Minimisation
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef REFINE_CGRAD_H
#define REFINE_CGRAD_H

#include "model.h"
#include "reflist.h"
#include "refine.h"

extern void refine_cgrad(AtomicModel *model, ReflectionList *reflections, RefinementSpec spec);

#endif	/* REFINE_CGRAD_H */

