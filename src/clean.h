/*
 * clean.h
 *
 * CLEAN Algorithm
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  Synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CLEAN_H
#define CLEAN_H

#include <gtk/gtk.h>

#include "reflist.h"

extern void clean_dialog_open(GtkWidget *widget, gpointer data);
extern void clean_aperture_open(ReflectionList *reflections);

#endif	/* CLEAN_H */

