/*
 * colwheel.h
 *
 * Colour wheel definition and visualisation
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  Synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef COLWHEEL_H
#define COLWHEEL_H

#include <gtk/gtk.h>

extern gint colwheel_show(GtkWidget *widget, gpointer data);
extern double colwheel_red(double am, double ph);
extern double colwheel_green(double am, double ph);
extern double colwheel_blue(double am, double ph);

#endif	/* COLWHEEL_H */
