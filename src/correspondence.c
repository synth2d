/*
 * correspondence.c
 *
 * Plot of calculated against measured structure factors
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "data.h"
#include "main.h"
#include "statistics.h"
#include "displaywindow.h"
#include "model.h"

static void correspondence_gpwrite(FILE *gnuplot, const char *string) {
	fwrite(string, strlen(string), 1, gnuplot);
}

void correspondence_show() {

	FILE *fh;
	unsigned int i;
	FILE *gnuplot;
	ReflectionList *reflections;
	ReflectionList *model_reflections;
	double scale;
	
	reflections = main_reflist();
	model_reflections = model_calculate_f(reflections, NULL, 69);
	
	scale = stat_scale(reflections, model_reflections);
	
	fh = fopen("synth2d-correspondence.dat", "w");
	for ( i=1; i<reflections->n_reflections; i++ ) {
		fprintf(fh, "%f %f\n", scale*model_reflections->refs[i].amplitude, reflections->refs[i].amplitude);
	}
	fclose(fh);
	
	gnuplot = popen("gnuplot -persist -", "w");
	if ( !gnuplot ) {
		error_report("Couldn't invoke gnuplot.  Please check your PATH.");
		return;
	}
	
	correspondence_gpwrite(gnuplot, "set autoscale\n");
	correspondence_gpwrite(gnuplot, "unset log\n");
	correspondence_gpwrite(gnuplot, "unset label\n");
	correspondence_gpwrite(gnuplot, "set xtic auto\n");
	correspondence_gpwrite(gnuplot, "set ytic auto\n");
	correspondence_gpwrite(gnuplot, "set grid\n");
	correspondence_gpwrite(gnuplot, "set ylabel '|Fobs|' font \"Helvetica,10\"\n");
	correspondence_gpwrite(gnuplot, "set xlabel '|Fcalc|' font \"Helvetica,10\"\n");
	correspondence_gpwrite(gnuplot, "set title 'Correspondence Plot' font \"Helvetica,10\"\n");
	correspondence_gpwrite(gnuplot, "plot [0:] [0:] 'synth2d-correspondence.dat'\n");
	correspondence_gpwrite(gnuplot, "replot\n");
	
	if ( pclose(gnuplot) == -1 ) {
		error_report("gnuplot returned an error code.");
		return;
	}
	
}
