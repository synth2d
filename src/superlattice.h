/*
 * superlattice.h
 *
 * Superlattice Operations
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *  Synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef SUPERLATTICE_H
#define SUPERLATTICE_H

#include "reflist.h"

extern ReflectionList *superlattice_split(ReflectionList *reflections, unsigned int xcells, unsigned int ycells);
extern void superlattice_split_open(void);

#endif	/* SUPERLATTICE_H */
