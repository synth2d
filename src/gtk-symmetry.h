/*
 * gtk-symmetry.h
 *
 * A simple widget to select a symmetry group
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifndef GTKSYMMETRY_H
#define GTKSYMMETRY_H

#include <gtk/gtk.h>

#include "symmetry.h"

typedef struct {

	GtkHBox parent;		/* Parent widget */
	
	unsigned int dimensions;	/* 1D, 2D, 3D, more...? */
	unsigned int trans_dimensions;	/* Number of dimensions of translational symmetry */
	gboolean has_friedel;
	GtkPositionType friedel_pos;
	
	GtkWidget *label;
	GtkWidget *selection;
	GtkWidget *table;
	GtkWidget *friedel;

} GtkSymmetry;

typedef struct {
	GtkHBoxClass parent_class;
	void (* changed) (GtkSymmetry *gtksymmetry);
} GtkSymmetryClass;

extern guint gtk_symmetry_get_type(void);
extern GtkWidget *gtk_symmetry_new(unsigned int dimensions, unsigned int trans_dimensions, gboolean has_friedel);
extern Symmetry gtk_symmetry_get_symmetry(GtkSymmetry *symmetry);
extern Symmetry gtk_symmetry_set_symmetry(GtkSymmetry *symmetry, Symmetry new_sym);

#define GTK_SYMMETRY(obj)		GTK_CHECK_CAST(obj, gtk_symmetry_get_type(), GtkSymmetry)
#define GTK_SYMMETRY_CLASS(class)	GTK_CHECK_CLASS_CAST(class, gtk_symmetry_get_type(), GtkSymmetryClass)
#define GTK_IS_SYMMETRY(obj)		GTK_CHECK_TYPE(obj, gtk_symmetry_get_type())

#endif /* GTKSYMMETRY_H */

