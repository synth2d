/*
 * refine.h
 *
 * Model Refintement
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef REFINE_H
#define REFINE_H

#include <gtk/gtk.h>

typedef enum {
	COORDINATE_X,
	COORDINATE_Y,
	COORDINATE_Z
} CoordinateSelector;

typedef enum {
	REFINE_SPEC_NONE = 0,
	REFINE_SPEC_X = 1<<0,
	REFINE_SPEC_Y = 1<<1,
	REFINE_SPEC_Z = 1<<2,
	REFINE_SPEC_B = 1<<3,
	REFINE_SPEC_OCC = 1<<4,
	REFINE_SPEC_THICKNESS = 1<<16,
	REFINE_SPEC_INTENSITIES = 1<<18
} RefinementSpec;

typedef enum {
	REFINE_TYPE_NONE = 0,
	REFINE_TYPE_NEIGHBOURSEARCH,
	REFINE_TYPE_BRENT,
	REFINE_TYPE_LMDER,
	REFINE_TYPE_LSQ,
	REFINE_TYPE_CGRAD
} RefinementType;

typedef struct {

	/* Model this refinement refers to */
	struct struct_atomicmodel *model;
	
	/* Refinement specifications */
	RefinementSpec spec;
	RefinementType type;

	/* Dialog box bits */
	GtkWidget *spec_x;
	GtkWidget *spec_y;
	GtkWidget *spec_z;
	GtkWidget *spec_b;
	GtkWidget *spec_thickness;
	GtkWidget *spec_occ;
	GtkWidget *type_neighboursearch;
	GtkWidget *type_brent;
	GtkWidget *type_lmder;
	GtkWidget *type_lsq;
	GtkWidget *type_cgrad;
	GtkWidget *nbsearch_shift;
	GtkWidget *target_amplitudes;
	GtkWidget *target_intensities;
	GtkWidget *stop;
	GtkWidget *go;

	/* Thread control */
	GThread *work_thread;
	unsigned int running;
	unsigned int run_semaphore;
	GStaticMutex display_mutex;
	guint display_callback;
	
} RefinementWindow;

#include "reflist.h"

typedef struct {
	struct struct_atomicmodel *model;
	ReflectionList *reflections;		/* Observations */
	unsigned int cur_mod_atom;
	CoordinateSelector cur_mod_coor;
	double scale;
	RefinementSpec spec;
} RefinementPair;

extern void refine_open(struct struct_atomicmodel *model);

#define LSQ_MSLS_SHIFT 0.000000001
#define MAX_REFINEMENT_ITERATIONS 400

extern void refine_schedule_update(struct struct_atomicmodel *model);

#endif	/* REFINE_H */

