/*
 * anneal.h
 *
 * Structure solution by simulated annealing
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef ANNEAL_H
#define ANNEAL_H

extern void anneal_dialog_open(void);

#endif	/* ANNEAL_H */
