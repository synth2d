/*
 * main.c
 *
 * Test harness
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdarg.h>

#include "data.h"
#include "reflist.h"

int main(int argc, char *argv[]) {

	int i;
	ReflectionList *reflist = NULL;

	if ( data_read(argv[1]) ) {
		return 1;
	}
	reflist = data_getreflections();
	if ( reflist == NULL ) {
		printf("data_getreflections() returned NULL\n");
		return 1;
	}
	
	printf("---------------- Initial list\n");
	printf("%i reflections\n", reflist->n_reflections);
	for ( i=0; i<reflist->n_reflections; i++ ) {
		printf("%3i %3i %3i %f\n", reflist->refs[i].h, reflist->refs[i].k, reflist->refs[i].l, reflist->refs[i].amplitude);
	}
	printf("---------------- Adding 200 = 1.5\n");
	reflist_addref_am(reflist, 2, 0, 0, 1.5);
	printf("%i reflections\n", reflist->n_reflections);
	for ( i=0; i<reflist->n_reflections; i++ ) {
		printf("%3i %3i %3i %f\n", reflist->refs[i].h, reflist->refs[i].k, reflist->refs[i].l, reflist->refs[i].amplitude);
	}
	printf("---------------- Removing 100\n");
	reflist_delref(reflist, 1, 0, 0);
	printf("%i reflections\n", reflist->n_reflections);
	for ( i=0; i<reflist->n_reflections; i++ ) {
		printf("%3i %3i %3i %f\n", reflist->refs[i].h, reflist->refs[i].k, reflist->refs[i].l, reflist->refs[i].amplitude);
	}
	printf("---------------- Trying to remove 000\n");
	reflist_delref(reflist, 0, 0, 0);
	printf("%i reflections\n", reflist->n_reflections);
	for ( i=0; i<reflist->n_reflections; i++ ) {
		printf("%3i %3i %3i %f\n", reflist->refs[i].h, reflist->refs[i].k, reflist->refs[i].l, reflist->refs[i].amplitude);
	}
	printf("---------------- Trying to add 000 = 5.0\n");
	reflist_addref_am(reflist, 0, 0, 0, 5.0);
	printf("%i reflections\n", reflist->n_reflections);
	for ( i=0; i<reflist->n_reflections; i++ ) {
		printf("%3i %3i %3i %f\n", reflist->refs[i].h, reflist->refs[i].k, reflist->refs[i].l, reflist->refs[i].amplitude);
	}
	
	return 0;

}

