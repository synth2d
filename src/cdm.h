/*
 * cdm.h
 *
 * "Conventional" Direct Methods
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CDM_H
#define CDM_H

#include <stdint.h>

#include "reflist.h"
#include "symmetry.h"

#define MAX_TRIPLETS 65536

typedef struct {
	Reflection p;
	Reflection q;
	/* The other reflection in the triplet is -p-q */
	double G;
} Triplet;

typedef struct {
	Triplet triplets[MAX_TRIPLETS];
	unsigned int n_triplets;
} TripletList;

typedef struct s_phase_sol {

	unsigned int n;			/* Reference number */
	unsigned int phasing_code;	/* Phase values, binary 1->Pi, 0->0.  Only works for centro */
	double unflatness;		/* Luzzatti 'unflatness': sigma(rho^4) */
	double entropy;			/* Entropy -sigma(P*ln(P)) */
	
	struct s_phase_sol *next;
	
} PhasingSolution;

typedef struct {

	unsigned int auto_iterate;
	unsigned int refine;
	double emin;
	double gmin;
	double amin;
	Symmetry sym;

	ReflectionList *reflections;		/* Overall list of reflections (used to store results) */
	ReflectionList *strongest_reflections;	/* Strongest reflections (used for phasing) */
	TripletList *triplet_list;		/* List of active triplets */
	ReflectionList *basis_list;		/* Starting set of reflections */
	
	GtkWidget *solution_tree_view;
	GtkListStore *solution_list_store;
	
	PhasingSolution *solutions;
	
	unsigned int n_assigned_basis;
	unsigned int n_assigned_expansion;
	unsigned int n_refined;

} CDMContext;

extern void cdm_dialog_open(void);
extern unsigned int cdm_tangentexpansion(CDMContext *cdm);
extern void cdm_display_phasing_solution(CDMContext *cdm, PhasingSolution *sol, ReflectionList *reflections);

#endif	/* CDM_H */
