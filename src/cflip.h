/*
 * cflip.h
 *
 * Charge -flipping algorithms
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 * (c) 2008 Alexander Eggeman <ase25@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CFLIP_H
#define CFLIP_H

#include "reflist.h"
#include "symmetry.h"
#include "gtk-symmetry.h"
#include "cdm.h"


extern void cflip_dialog_open(void);

#endif	/* CFLIP_H */
