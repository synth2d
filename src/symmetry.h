/*
 * symmetry.h
 *
 * Symmetry stuff
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef SYMMETRY_H
#define SYMMETRY_H

#include <fftw3.h>

#include "reflist.h"

/* Symmetry elements */
typedef enum {
	SYMMETRY_IDENTITY			= 0,
	SYMMETRY_FRIEDEL			= 1<<0,
	SYMMETRY_MIRROR_DIAGONAL		= 1<<1,
	SYMMETRY_CENTRE				= 1<<2,
	SYMMETRY_GLIDE_HORIZONTAL_QUARTER	= 1<<3,
	SYMMETRY_GLIDE_VERTICAL_QUARTER		= 1<<4,
	SYMMETRY_ROTATION_4			= 1<<5,
	SYMMETRY_GLIDE_DIAGONAL			= 1<<6,
	SYMMETRY_MIRROR_VERTICAL		= 1<<7,
	SYMMETRY_MIRROR_HORIZONTAL		= 1<<8,
	SYMMETRY_GLIDE_HORIZONTAL		= 1<<9,
	SYMMETRY_GLIDE_VERTICAL			= 1<<10,
	SYMMETRY_TRIAD				= 1<<11,
	SYMMETRY_MIRROR_VERTICAL_QUARTER	= 1<<12,
	SYMMETRY_MIRROR_HORIZONTAL_QUARTER	= 1<<13
} Symmetry;

/* Atoms at cell edges and corners need to be duplicated when displaying the model,
	but not when calculating structure factors. */
typedef enum {
	MODEL_TARGET_DISPLAY,
	MODEL_TARGET_CALCULATION
} ModelTarget;

/* Planegroups */
#define PLANEGROUP_P1 (SYMMETRY_IDENTITY)
#define PLANEGROUP_P2 (SYMMETRY_CENTRE)
#define PLANEGROUP_PM_X (SYMMETRY_MIRROR_HORIZONTAL)
#define PLANEGROUP_PM_Y (SYMMETRY_MIRROR_VERTICAL)
#define PLANEGROUP_PG_X (SYMMETRY_GLIDE_HORIZONTAL)
#define PLANEGROUP_PG_Y (SYMMETRY_GLIDE_VERTICAL)
#define PLANEGROUP_CM_X (SYMMETRY_MIRROR_HORIZONTAL | SYMMETRY_GLIDE_HORIZONTAL_QUARTER)
#define PLANEGROUP_CM_Y (SYMMETRY_MIRROR_VERTICAL | SYMMETRY_GLIDE_VERTICAL_QUARTER)
#define PLANEGROUP_P2MM (SYMMETRY_CENTRE | SYMMETRY_MIRROR_HORIZONTAL | SYMMETRY_MIRROR_VERTICAL)
#define PLANEGROUP_P2MG_X (SYMMETRY_CENTRE | SYMMETRY_GLIDE_VERTICAL | SYMMETRY_MIRROR_HORIZONTAL_QUARTER)
#define PLANEGROUP_P2MG_Y (SYMMETRY_CENTRE | SYMMETRY_GLIDE_HORIZONTAL | SYMMETRY_MIRROR_VERTICAL_QUARTER)
#define PLANEGROUP_P2GG (SYMMETRY_CENTRE | SYMMETRY_GLIDE_HORIZONTAL_QUARTER | SYMMETRY_GLIDE_VERTICAL_QUARTER)
#define PLANEGROUP_C2MM (SYMMETRY_CENTRE | SYMMETRY_GLIDE_HORIZONTAL_QUARTER | SYMMETRY_GLIDE_VERTICAL_QUARTER \
				| SYMMETRY_MIRROR_HORIZONTAL | SYMMETRY_MIRROR_VERTICAL)
#define PLANEGROUP_P4 (SYMMETRY_CENTRE | SYMMETRY_ROTATION_4)
#define PLANEGROUP_P4MM (SYMMETRY_CENTRE | SYMMETRY_MIRROR_DIAGONAL | SYMMETRY_ROTATION_4) /* Implies the other diagonal */
#define PLANEGROUP_P4GM (SYMMETRY_CENTRE | SYMMETRY_ROTATION_4 | SYMMETRY_GLIDE_DIAGONAL)  /*| SYMMETRY_GLIDE_HORIZONTAL_QUARTER */
#define PLANEGROUP_P3 (SYMMETRY_TRIAD)
#define PLANEGROUP_P3M1 (SYMMETRY_TRIAD | SYMMETRY_MIRROR_DIAGONAL)
#define PLANEGROUP_P31M (SYMMETRY_TRIAD | SYMMETRY_MIRROR_HORIZONTAL)
#define PLANEGROUP_P6 (SYMMETRY_TRIAD | SYMMETRY_CENTRE)
#define PLANEGROUP_P6MM (SYMMETRY_TRIAD | SYMMETRY_CENTRE | SYMMETRY_MIRROR_VERTICAL | SYMMETRY_MIRROR_HORIZONTAL)

typedef enum {
	SYMFLAG_NONE				= 0,
	SYMFLAG_PHASES_KNOWN			= 1<<0,
	SYMFLAG_PHASES_CALC			= 1<<1,
} SymFlags;

/* Functions */
extern struct struct_atomicmodel *symmetry_generate_equivalent_atoms(struct struct_atomicmodel *model, size_t j, ModelTarget target);
extern ReflectionList *symmetry_generate_equivalent_reflections(Symmetry sym, signed int h, signed int k, signed int l);
extern ReflectionList symmetry_reduce(ReflectionList *input, Symmetry sym);
extern double symmetry_symmetrise(ReflectionList *input, Symmetry sym, SymFlags flags);	/* Returns R_sym */
extern const char *symmetry_decode(Symmetry sym);
extern Symmetry symmetry_encode(const char *symmetry);
extern void symmetry_centricity(ReflectionList *reflections, unsigned int i, Symmetry sym, SymFlags flags);
extern void symmetry_symmetrise_array(fftw_complex *in, signed int width, signed int height, Symmetry sym);

#endif	/* SYMMETRY_H */

