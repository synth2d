/*
 * superlattice.c
 *
 * Superlattice Operations
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

#include "displaywindow.h"
#include "main.h"
#include "reflist.h"
#include "data.h"

typedef struct {

	GtkWidget *window;
	GtkWidget *xcells;
	GtkWidget *ycells;
	
} SuperlatticeSplitWindow;

SuperlatticeSplitWindow *superlattice_sls = NULL;

ReflectionList *superlattice_split(ReflectionList *old, unsigned int xcells, unsigned int ycells) {

	ReflectionList *new;
	unsigned int i;
	
	printf("SL: Splitting %ix%i superlattice cell\n", xcells, ycells);
	
	new = reflist_new_parent(old);
	for ( i=1; i<old->n_reflections; i++ ) {
	
		signed int h, k, l;
		
		h = old->refs[i].h;  k = old->refs[i].k;  l = old->refs[i].l;
		if ( (h % (signed)xcells == 0) && (k % (signed)ycells == 0) ) {
			reflist_addref_am_parent(new, h/(signed)xcells, k/(signed)ycells, l, old->refs[i].amplitude, i);
		}
		
	}
	data_dividecell(xcells, ycells, 1);
	displaywindow_createfourier();
	displaywindow_kicksize();
	
	return new;

}

static gint superlattice_split_response(GtkWidget *widget, gint response, SuperlatticeSplitWindow *sls) {

	int done = 1;

	if ( response == GTK_RESPONSE_OK ) {
		const char *xcells;
		const char *ycells;
		unsigned int xc, yc;
		int scanval;
		xcells = gtk_entry_get_text(GTK_ENTRY(sls->xcells));
		ycells = gtk_entry_get_text(GTK_ENTRY(sls->ycells));
		scanval = sscanf(xcells, "%u", &xc);
		scanval += sscanf(ycells, "%u", &yc);
		if ( scanval != 2 ) {
			error_report("Please enter valid values for both superlattice cell dimensions.");
			done = 0;
		} else {
			main_superlattice_split(xc, yc);
		}
	}

	if ( done ) {
		gtk_widget_destroy(sls->window);
		free(sls);
		superlattice_sls = NULL;
	}
	
	return 0;
}

void superlattice_split_open() {

	SuperlatticeSplitWindow *sls;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *table;
	GtkWidget *xcells_label;
	GtkWidget *ycells_label;
	
	if ( superlattice_sls ) {
		return;
	}
	sls = malloc(sizeof(SuperlatticeSplitWindow));
	superlattice_sls = sls;
	
	sls->window = gtk_dialog_new_with_buttons("Superlattice Split", GTK_WINDOW(displaywindow_gtkwindow()),
		GTK_DIALOG_DESTROY_WITH_PARENT,	GTK_STOCK_CANCEL, GTK_RESPONSE_CLOSE, GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	
	vbox = gtk_vbox_new(FALSE, 0);
	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(sls->window)->vbox), GTK_WIDGET(hbox), FALSE, FALSE, 7);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(vbox), FALSE, FALSE, 5);
	
	table = gtk_table_new(2, 2, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5);
	gtk_table_set_col_spacings(GTK_TABLE(table), 5);
	gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(table), FALSE, FALSE, 0);
		
	xcells_label = gtk_label_new("Number of cells along x:");
	gtk_misc_set_alignment(GTK_MISC(xcells_label), 1, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(table), GTK_WIDGET(xcells_label), 1, 2, 1, 2);
	
	sls->xcells = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(superlattice_sls->xcells), "1");
	gtk_table_attach_defaults(GTK_TABLE(table), GTK_WIDGET(sls->xcells), 2, 3, 1, 2);
	
	ycells_label = gtk_label_new("Number of cells along y:");
	gtk_misc_set_alignment(GTK_MISC(ycells_label), 1, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(table), GTK_WIDGET(ycells_label), 1, 2, 2, 3);
	
	sls->ycells = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(sls->ycells), "1");
	gtk_table_attach_defaults(GTK_TABLE(table), GTK_WIDGET(sls->ycells), 2, 3, 2, 3);

	g_signal_connect(G_OBJECT(sls->window), "response", G_CALLBACK(superlattice_split_response), sls);
	gtk_widget_show_all(sls->window);
	gtk_widget_grab_focus(GTK_WIDGET(sls->xcells));

}

