/*
 * elements.c
 *
 * Elemental Data
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2D - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "elements.h"

Element elements[255];
static unsigned int elements_initialised = 0;

void elements_initialise() {

	int i, whoops;
	FILE *fh;

	if ( elements_initialised ) return;

	i = 0;  whoops = 0;
	fh = fopen(DATADIR"/synth2d/elements", "r");
	do {

		char line[512];
		char buf[512];
		float a1, b1, a2, b2, a3, b3, a4, b4, c;

		fgets(line, 511, fh);
		if ( ferror(fh) || feof(fh) ) {
			whoops = 1;
			break;
		}
		if ( strlen(line) > 1 ) {

			if ( line[strlen(line)-1] == '\n' ) {
				line[strlen(line)-1] = '\0';
			}

			sscanf(line, "%s\t%i\t%9f\t%9f\t%9f\t%9f\t%9f\t%9f\t%9f\t%9f\t%9f", buf, &elements[i].z, &a1, &b1, &a2, &b2, &a3, &b3, &a4, &b4, &c);
			elements[i].element_name = strdup(buf);
			elements[i].sfac_a1 = a1;  elements[i].sfac_b1 = b1;
			elements[i].sfac_a2 = a2;  elements[i].sfac_b2 = b2;
			elements[i].sfac_a3 = a3;  elements[i].sfac_b3 = b3;
			elements[i].sfac_a4 = a4;  elements[i].sfac_b4 = b4;
			elements[i].sfac_c = c;
			
			i++;
		
		} else {
			line[0] = '\0';
		}
		
	}  while ( !whoops && !feof(fh) );

	elements[i].element_name = "EOF";
	elements_initialised = 1;
	
}

unsigned int elements_lookup(const char *name) {

	size_t i;
	
	i = 0;
	while ( strcmp(elements[i].element_name, "EOF") != 0 ) {
		if ( strcmp(elements[i].element_name, name) == 0 ) return i;
		i++;
	}
	
	fprintf(stderr, "Failed to look up element\n");
	return i;

}

