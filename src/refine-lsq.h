/*
 * refine-lsq.h
 *
 * Refinement by Simple LSQ
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef REFINE_LSQ_H
#define REFINE_LSQ_H

#include "model.h"
#include "reflist.h"
#include "refine.h"

extern void refine_lsq(AtomicModel *model_orig, ReflectionList *reflections, RefinementSpec spec);

#endif	/* REFINE_LSQ_H */

