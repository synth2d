/*
 * statistics.h
 *
 * Structure Factor Statistics
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef STATISTICS_H
#define STATISTICS_H

#include "reflist.h"

extern double stat_scale(ReflectionList *obs, ReflectionList *calc);
extern double stat_scale_intensity(ReflectionList *obs, ReflectionList *calc);

extern double stat_r1(ReflectionList *a, ReflectionList *b);
extern double stat_r2(ReflectionList *a, ReflectionList *b);
#define stat_r(a, b) stat_r1((a), (b))

extern double stat_sigma_f(ReflectionList *reflections);
extern double stat_stddev(ReflectionList *a);
extern double stat_maxam(ReflectionList *a);

#endif	/* STATISTICS_H */
