/*
 * model.h
 *
 * Atomic Model Structures
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifndef MODEL_H
#define MODEL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "symmetry.h"
#include "reflist.h"
#include "model-editor.h"
#include "refine.h"

#define MAX_ATOMS 255

typedef struct {

	unsigned int		ref;	/* Atom type reference */
	double			x;
	double			y;
	double			z;
	double			B;		/* Debye-Waller thermal parameter */
	double			occ;		/* Site occupancy */
	
	unsigned int		refine;	/* Refine this atom? */
	unsigned int		active;	/* Include this atom in calculations? */
		
} ModelAtom;

typedef struct struct_atomicmodel {

	Symmetry		sym;
	double			thickness;
	int			point_atoms;
	
	ModelEditor		*editor;			/* Editor for this model, if open */
	RefinementWindow	*refine_window;	/* Refinement window for this model, if open */
	int			lock_count;
	
	unsigned int		n_atoms;
	ModelAtom		atoms[MAX_ATOMS];
	
} AtomicModel;

extern AtomicModel *model_new(void);
extern AtomicModel *model_copy(const AtomicModel *a);
extern void model_move(AtomicModel *a, AtomicModel *b);
extern void model_free(AtomicModel *model);

extern AtomicModel *model_load(const char *filename);
extern void model_load_as_current(const char *filename);
extern void model_save(const char *filename, AtomicModel *model);

extern ReflectionList *model_calculate_f(ReflectionList *template, AtomicModel *given_model, signed int layer);
extern void model_calculate_difference_coefficients(ReflectionList *reflections);
extern void model_calculate_refinement_coefficients(ReflectionList *reflections);
extern double model_mod_f(AtomicModel *model, signed int h, signed int k, signed int l);

extern AtomicModel *model_get_current();
extern void model_open_editor(void);
extern void model_notify_update(AtomicModel *model);
extern void model_notify_update_editor(AtomicModel *model);
extern void model_default(void);
extern void model_lock(AtomicModel *model);
extern void model_unlock(AtomicModel *model);
extern int model_current_is_blank(void);

#endif	/* MODEL_H */

