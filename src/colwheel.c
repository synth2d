/*
 * colwheel.c
 *
 * Colour wheel definition and visualisation
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  Synth2D - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>
#include <fftw3.h>

#include "displaywindow.h"

#define COLWHEEL_WIDTH 256
#define COLWHEEL_SIZE ((COLWHEEL_WIDTH)-1)
#define COLWHEEL_HALF ((COLWHEEL_WIDTH/2)-1)

#define rad2deg(a) ((a)*180.0/M_PI)
#define deg2rad(a) ((a)*M_PI/180.0)

gint colwheel_show(GtkWidget *widget, gpointer data) {

	GtkWidget *colwheel_window;
	GdkPixbuf *colwheel_pixbuf;
	GtkWidget *colwheel_pixmap_widget;
	fftw_complex *colwheel;

	unsigned int x, y;
	double am, ph;

	/* This isn't being transformed, so no need to use fftw_malloc() */
	colwheel = malloc(COLWHEEL_SIZE*COLWHEEL_SIZE*sizeof(fftw_complex));
	for ( x=0; x<COLWHEEL_SIZE; x++ ) {
		for ( y=0; y<COLWHEEL_SIZE; y++ ) {
			colwheel[y + COLWHEEL_SIZE*x][0] = 0;
			colwheel[y + COLWHEEL_SIZE*x][1] = 0;
		}
	}

	/* Plot out a disc containing smoothly varying amplitude and phase */
	for ( am=0.0; am<1.0; am+=(1.0/COLWHEEL_SIZE) ) {
		for ( ph=0.0; ph<2.0*M_PI; ph+=1.0/(2.0*M_PI*(double)COLWHEEL_SIZE) ) {
			x = (unsigned int)(COLWHEEL_HALF + (COLWHEEL_HALF*am*cos(ph)));
			y = (unsigned int)(COLWHEEL_HALF + (COLWHEEL_HALF*am*sin(ph)));
			colwheel[(COLWHEEL_SIZE-1-y) + COLWHEEL_SIZE*(COLWHEEL_SIZE-1-x)][0] = am*cos(ph);
			colwheel[(COLWHEEL_SIZE-1-y) + COLWHEEL_SIZE*(COLWHEEL_SIZE-1-x)][1] = am*sin(ph);
		}
	}

	/* Draw a line to remind the user where zero phase is */
	for ( x=COLWHEEL_HALF+1; x<COLWHEEL_SIZE; x++ ) {
		colwheel[COLWHEEL_HALF + COLWHEEL_SIZE*(COLWHEEL_SIZE-1-x)][0] = 0;
		colwheel[COLWHEEL_HALF + COLWHEEL_SIZE*(COLWHEEL_SIZE-1-x)][1] = 0;
	}

	colwheel_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(colwheel_window), "Colour Wheel");
	colwheel_pixbuf = displaywindow_render_pixbuf(colwheel, 1, COLWHEEL_SIZE, COLWHEEL_SIZE, M_PI_2, 1, 1);
	free(colwheel);
	colwheel_pixmap_widget = gtk_image_new_from_pixbuf(colwheel_pixbuf);
	gtk_container_add(GTK_CONTAINER(colwheel_window), colwheel_pixmap_widget);
	gtk_widget_show_all(colwheel_window);

	return 0;

}

double colwheel_blue(double am, double ph) {

	double f;
	unsigned int hi;
	double d;

	ph = rad2deg(ph);		/* Convert to degrees */
	ph += 180.0;			/* Rotation of colour wheel */
	if ( ph >= 360.0 ) ph = 0.0;
	d = ph / 60.0;

	hi = floor(d);	/* Divide the colour wheel into six sections */
	f = d - hi;	/* Distance into the current section of the colour wheel */
	if ( hi == 6 ) hi = 0;
	switch ( hi ) {
		case 0 : return am;
		case 1 : return am;
		case 2 : return am*(1.0-f);
		case 3 : return 0.0;
		case 4 : return am*f/2.0;
		case 5 : return am/2.0+am*f/2.0;
		default : return 0.0;
	}

}

double colwheel_green(double am, double ph) {

	double f;
	unsigned int hi;
	double d;

	ph = rad2deg(ph);		/* Convert to degrees */
	ph += 180.0;			/* Rotation of colour wheel */
	if ( ph >= 360.0 ) ph = 0.0;
	d = ph / 60.0;

	hi = floor(d);	/* Divide the colour wheel into six sections */
	f = d - hi;	/* Distance into the current section of the colour wheel */
	if ( hi == 6 ) hi = 0;
	switch ( hi ) {
		case 0 : return am*f/2.0;
		case 1 : return am/2.0+am*f/2.0;
		case 2 : return am;
		case 3 : return am*(1.0-f);
		case 4 : return 0.0;
		case 5 : return 0.0;
		default : return 0.0;
	}

}

double colwheel_red(double am, double ph) {

	double f;
	unsigned int hi;
	double d;

	ph = rad2deg(ph);		/* Convert to degrees */
	ph += 180.0;			/* Rotation of colour wheel */
	if ( ph >= 360.0 ) ph = 0.0;
	d = ph / 60.0;

	hi = floor(d);	/* Divide the colour wheel into six sections */
	f = d - hi;	/* Distance into the current section of the colour wheel */
	if ( hi == 6 ) hi = 0;
	switch ( hi ) {
		case 0 : return 0.0;
		case 1 : return 0.0;
		case 2 : return 0.0;
		case 3 : return am*f;
		case 4 : return am;
		case 5 : return am*(1.0-f);
		default : return 0.0;
	}

}
