/*
 * png-file.h
 *
 * PNG output
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef PNGFILE_H
#define PNGFILE_H

#include <fftw3.h>

extern int png_write(const char *filename, fftw_complex *out, double norm, int nx, int ny);

#endif	/* PNGFILE_H */
