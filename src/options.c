/*
 * options.c
 *
 * Handle run-time options and stuff
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <glob.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <fftw3.h>

static void options_createhomedir(char *filename) {

	/* Create directory. */
	if ( mkdir(filename, S_IRUSR | S_IWUSR | S_IXUSR) != 0 ) {
		fprintf(stderr, "OP: Couldn't create ~/.synth2d directory.\n");
		exit(1);
	}

}

static char *options_checkdir() {

	int glob_retval;
	glob_t glob_result;
	struct stat stat_buffer;
	struct stat *statbuf;
	char *dir_filename;
	
	glob_retval = glob("~", GLOB_TILDE, NULL, &glob_result);
	statbuf = &stat_buffer;
	if ( glob_retval != 0 ) {

		fprintf(stderr, "OP: glob() for ~/.synth2d failed: ");
		switch ( glob_retval ) {
			case GLOB_NOSPACE : fprintf(stderr, "GLOB_NOSPACE\n"); break;
			case GLOB_ABORTED : fprintf(stderr, "GLOB_ABORTED\n"); break;
			case GLOB_NOMATCH : fprintf(stderr, "GLOB_NOMATCH\n"); break;
			default	: fprintf(stderr, "Unknown!\n"); break;
		}
		return NULL;

	}
	
	dir_filename = malloc(strlen(glob_result.gl_pathv[0]) + 12);
	strcpy(dir_filename, glob_result.gl_pathv[0]);
	globfree(&glob_result);
	strcat(dir_filename, "/.synth2d");
	
	if ( stat(dir_filename, statbuf) != -1 ) {
	
		if ( S_ISDIR(stat_buffer.st_mode) ) {
	
			//printf("OP: Found '%s'.\n", dir_filename);
			
		} else {
		
			fprintf(stderr, "OP: Found '%s', but it isn't a directory!\n", dir_filename);
			return NULL;
			
		}
		
	} else {
	
		printf("OP: ~/.synth2d directory not found: creating it.\n");
		options_createhomedir(dir_filename);
		
	}
	
	return dir_filename;
	
}


void options_load() {

	char *dir_filename;
	char *rc_filename;
	char *wisdom_filename;
	struct stat stat_buffer;
	struct stat *statbuf;
	
	dir_filename = options_checkdir();
	
	rc_filename = malloc(strlen(dir_filename) + 8);
	strcpy(rc_filename, dir_filename);
	strcat(rc_filename, "/config");
	statbuf = &stat_buffer;
	if ( stat(rc_filename, statbuf) != -1 ) {
	
		if ( (S_ISREG(stat_buffer.st_mode)) || (S_ISLNK(stat_buffer.st_mode)) ) {

		
		} else {
		
			fprintf(stderr, "OP: Config file isn't a regular file or a link.  Giving up!\n");
			exit(1);
		
		}
		
	} else {
		printf("OP: Config file doesn't exist.  This is OK.\n");
	}
	free(rc_filename);
	
	wisdom_filename = malloc(strlen(dir_filename) + 13);
	strcpy(wisdom_filename, dir_filename);
	strcat(wisdom_filename, "/fftw-wisdom");
	if ( stat(wisdom_filename, statbuf) != -1 ) {
	
		if ( (S_ISREG(stat_buffer.st_mode)) || (S_ISLNK(stat_buffer.st_mode)) ) {

			FILE *fh;
			
			fh = fopen(wisdom_filename, "r");
			if ( fh == NULL ) {			
				fprintf(stderr, "OP: Error opening FFTW wisdom file.\n");
			} else {
				if ( fftw_import_wisdom_from_file(fh) == 1 ) {
					//printf("OP: Successfully imported FFTW wisdom\n");
				} else {
					fprintf(stderr, "OP: Failed to import FFTW wisdom\n");
				}
			}
			
			fclose(fh);
		
		} else {
		
			fprintf(stderr, "OP: FFTW wisdom file isn't a regular file or a link.  Giving up!\n");
			exit(1);
		
		}
		
	} else {
		printf("OP: FFTW wisdom file doesn't exist.  This is OK.\n");
	}
	free(wisdom_filename);
	
	free(dir_filename);

}

void options_save() {

	char *dir_filename;
	char *rc_filename;
	char *wisdom_filename;
	FILE *fh;
	
	dir_filename = options_checkdir();
	
	rc_filename = malloc(strlen(dir_filename) + 8);
	strcpy(rc_filename, dir_filename);
	strcat(rc_filename, "/config");
	fh = fopen(rc_filename, "w");
	if ( fh == NULL ) {			
		fprintf(stderr, "OP: Error opening options file.\n");
	} else {
		fprintf(fh, "# synth2d options file\n");
	}
	free(rc_filename);
	
	wisdom_filename = malloc(strlen(dir_filename) + 13);
	strcpy(wisdom_filename, dir_filename);
	strcat(wisdom_filename, "/fftw-wisdom");
	fh = fopen(wisdom_filename, "w");
	if ( fh == NULL ) {			
		fprintf(stderr, "OP: Error opening options file.\n");
	} else {
		fftw_export_wisdom_to_file(fh);
	}
	fclose(fh);
	free(wisdom_filename);
	
	free(dir_filename);
	
}

