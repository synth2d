/*
 * correspondence.h
 *
 * Plot of calculated against measured structure factors
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef CORRESPONDENCE_H
#define CORRESPONDENCE_H

extern void correspondence_show(void);

#endif	/* CORRESPONDENCE_H */
