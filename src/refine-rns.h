/*
 * refine-rns.h
 *
 * Refinement by Random Neighbour Search
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef REFINE_RNS_H
#define REFINE_RNS_H

#include "model.h"
#include "reflist.h"
#include "refine.h"

extern void refine_neighboursearch(AtomicModel *model, ReflectionList *reflections, RefinementSpec spec, double shift);

#endif	/* REFINE_RNS_H */

