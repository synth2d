/*
 * elser.h
 *
 * Elser Difference Map Algorithm
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef ELSER_H
#define ELSER_H

#include "reflist.h"

extern void elser_dialog_open();
extern void elser_grab_phases(ReflectionList *reflections);

#endif	/* ELSER_H */

