/*
 * refine-lmder.h
 *
 * Refinement by GSL's Levenberg-Marquardt LSQ
 *
 * (c) 2006-2007 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - Two-Dimensional Crystallographic Fourier Synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef REFINE_LMDER_H
#define REFINE_LMDER_H

#include "model.h"
#include "reflist.h"
#include "refine.h"

extern void refine_lmder(AtomicModel *model, ReflectionList *reflections, RefinementSpec spec);

#endif	/* REFINE_LMDER_H */

