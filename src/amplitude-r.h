/*
 * ampltude-r.h
 *
 * Plot of R-factor against amplitude
 *
 * (c) 2006 Thomas White <taw27@cam.ac.uk>
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef AMPLITUDER_H

extern void amplituder_show(void);

#endif	/* AMPLITUDER_H */
