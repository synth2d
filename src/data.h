/*
 * data.h
 *
 * Handle the input data
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  Synth2d - two-dimensional Fourier synthesis
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef DATA_H
#define DATA_H

#include "reflist.h"

extern double data_a();
extern double data_b();
extern double data_c();
extern double data_gamma();
extern unsigned int data_width(void);
extern unsigned int data_height(void);
unsigned int data_get_image_scale(void);
extern void data_dividecell(unsigned int na, unsigned int nb, unsigned int nc);
extern int data_read(const char *filename);
extern ReflectionList *data_getreflections(void);
extern void data_free();

#endif	/* DATA_H */

