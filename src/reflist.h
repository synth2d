/*
 * reflist.h
 *
 * Reflection-handling code
 *
 * (c) 2006-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  synth2d - two-dimensional Fourier synthesis
 *
 */

#ifndef REFLIST_H
#define REFLIST_H

#include <fftw3.h>

#define MAX_REFLECTIONS 20000

typedef struct {

	signed int h;
	signed int k;
	signed int l;

	/* Some, all or none of the following may be filled in, depending on the context */
	
	double amplitude;		/* Structure amplitude F (not I=F^2).  Must be positive */
	double am_error;		/* Standard error in amplitude */
	double weight;			/* Statistical weight for this data point */

	double phase_known;		/* Phase known a priori */
	unsigned int phase_known_set;
	double phase_calc;		/* Phase calculated */
	unsigned int phase_calc_set;
	
	double re;
	double im;			/* LSQ routine does crazy things with these */

	double alpha;			/* For convergence procedure and tangent refinement */
	double delta_theta;		/* Some kind of phase relationship, */
	signed int multiplier;		/*	for symmetry stuff          */
	
	double amplitude_dyn;
	double phase_dyn;		/* As calculated by the multislice routine */
	
	unsigned int parent_index;	/* Position of the same reflection in a related list */
	
} Reflection;

typedef struct _reflectionlist {
	Reflection refs[MAX_REFLECTIONS];
	unsigned int n_reflections;
	struct _reflectionlist *parent;
	double scale;
} ReflectionList;

typedef struct {
	ReflectionList *a;
	ReflectionList *b;
} ReflectionListPair;

extern ReflectionList *reflist_new(void);
extern ReflectionList *reflist_new_parent(ReflectionList *parent);
extern ReflectionList *reflist_copy(ReflectionList *list);
extern void reflist_free(ReflectionList *list);

extern int reflist_addref(ReflectionList *list, signed int h, signed int k, signed int l);
extern int reflist_addref_parent(ReflectionList *list, signed int h, signed int k, signed int l, unsigned int parent_index);
extern int reflist_addref_am(ReflectionList *list, signed int h, signed int k, signed int l, double am);
extern int reflist_addref_am_ph(ReflectionList *list, signed int h, signed int k, signed int l, double am, double ph);
extern int reflist_addref_alpha_parent(ReflectionList *list, signed int h, signed int k, signed int l, double alpha, unsigned int parent_index);
extern int reflist_addref_phase(ReflectionList *list, signed int h, signed int k, signed int l, double phase);
extern int reflist_addref_deltatheta(ReflectionList *list, signed int h, signed int k, signed int l, double delta_theta, signed int multiplier);
extern void reflist_delref(ReflectionList *list, signed int h, signed int k, signed int l);
extern int reflist_inlist(const ReflectionList *list, signed int h, signed int k, signed int l);
extern int reflist_inlist_2d(const ReflectionList *list, signed int h, signed int k);
extern void reflist_set_components(ReflectionList *list, signed int h, signed int k, signed int l, double re, double im);
extern int reflist_addref_am_parent(ReflectionList *list, signed int h, signed int k, signed int l, double am, unsigned int parent_index);

extern ReflectionList *reflist_new_from_array(fftw_complex *array, int width, int height);
extern void reflist_fill_array(fftw_complex *array, const ReflectionList *reflections, int width, int height);

#endif	/* REFLIST_H */
